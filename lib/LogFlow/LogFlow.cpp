/*
    Project: LogFlow
    File: LogFlow.cpp
    Author: @cytotv
*/

#include <LogFlow.h>

void LogFlow::init(int speed)
{
    Serial.begin(speed);
    log("LogFlow was initialized successfully. Version: " + String(VERSION), LF_SYSTEM);
    log("I'll written on " + String(speed) + " bauds", LF_SYSTEM);
}

void LogFlow::setActive(bool _isActive)
{
    isActive = _isActive;
}

void LogFlow::log(String text)
{
    log(text, "INFO");
}

void LogFlow::log(String text, String type)
{
    if(!isActive) return;
    type.toUpperCase();
    Serial.println("[" + type + "]: " + text);
}

void LogFlow::log(String text, byte type)
{
    switch(type)
    {
        case LF_SYSTEM:
            log(text, "LogFlow");
            break;

        case LF_WARN:
            log(text, "WARNING");
            break;
        
        case LF_ERR:
            log(text, "ERROR");
            break;

        default:
            log(text, "INFO");
            break;
    }
}