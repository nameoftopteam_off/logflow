/*
    Project: LogFlow
    File: LogFlow.h
    Author: @cytotv
*/

#include <Arduino.h>

// Information about build
#define VERSION 1.03

// Types of message
#define LF_SYSTEM 0
#define LF_INFO 1
#define LF_WARN 2
#define LF_ERR 3

class LogFlow
{
    private:
        bool isActive = true;
    public:
        void init(int speed);
        void setActive(bool _isActive);
        void log(String text);
        void log(String text, String type);
        void log(String text, byte type);
};